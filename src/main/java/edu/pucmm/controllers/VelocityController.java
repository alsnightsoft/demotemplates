package edu.pucmm.controllers;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;

@RestController
public class VelocityController {

    @RequestMapping("/demo/spring/velocity")
    public String index() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/VelocityDemo.html");
        VelocityContext context = new VelocityContext();
        context.put("name", "Curso de programacion web II");
        context.put("funciona", "Claro que si funciona!");
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
