package edu.pucmm.controllers;

import edu.pucmm.utils.URL;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FreeMarkerController {

    @RequestMapping("/demo/spring/freemarker")
    public String index() {
        try {
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
            cfg.setDirectoryForTemplateLoading(new File("html/FreeMarkerDemo.html"));
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            Map<String, Object> map = new HashMap<>();
            map.put("blogTitle", "Freemarker Template Demo");
            map.put("message", "Getting started with Freemarker.<br/>Find a simple Freemarker demo.");
            List<URL> references = new ArrayList<>();
            references.add(new URL("http://url1.com", "URL One"));
            references.add(new URL("http://url2.com", "URL Two"));
            references.add(new URL("http://url3.com", "URL Three"));
            map.put("references", references);
            Template template = cfg.getTemplate("blog-template.ftl");
            StringWriter writer = new StringWriter();
            template.process(map, writer);
            writer.flush();
            return writer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "Error en el template";
    }
}
