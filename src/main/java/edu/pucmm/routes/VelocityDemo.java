package edu.pucmm.routes;

import edu.pucmm.model.RouteBase;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import spark.Request;
import spark.Response;

import java.io.StringWriter;

/**
 * @author a.marte
 */
public class VelocityDemo extends RouteBase {

    public VelocityDemo() {
        super("/demo/spark/velocity");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/VelocityDemo.html");
        VelocityContext context = new VelocityContext();
        context.put("name", "Curso de programacion web II");
        context.put("funciona", "Claro que si funciona!");
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}
