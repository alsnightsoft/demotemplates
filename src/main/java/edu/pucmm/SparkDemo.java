package edu.pucmm;

import edu.pucmm.model.RouteBase;
import edu.pucmm.routes.FreeMarkerDemo;
import edu.pucmm.routes.VelocityDemo;
import spark.Spark;

public class SparkDemo {

    public static void main(String[] args) {
        Spark.port(8070);
        addRoute(new FreeMarkerDemo());
        addRoute(new VelocityDemo());
        Spark.init();
    }

    private static void addRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }
}
